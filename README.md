# Text Based Game

A small text based adventure game written in c++ the player must input commands through the console to try and clear the game.

## **Exectue**

Clone project and compile the C++ code in your compiler of preference, and then run the executable.

## **Basic Command List**
*  `go`		/	place to go
*  `equip`		/	weapon name
*  `pickup` 	/ 	*(chest_name)	/ item name
*  `place`		/	item name
*  `read`
*  `look` (prints description of the room)
*  `help` (prints all the avaiable commands)
*  `save` (saves the game)
*  `load` (loads the game at last save)
*  `exit` (extis the game)
  
*If you type in the word:  **"help"** in game you can see all the commands avaiable to each room*

*For picking items from a chest you have to input pickup, the chest name and then the item you want. If you pickup from the ground its only pickup, item name.*

## **Limited Commands**
* `run`	/			/ (limited to room Jungle_01)
* `bow`	/ soldier	/ (limited to room Jungle_01)
* `hide`	/			/ (limited to room Jungle_01)
* `swim`	/ across	/ (limited to room River_01)
* `push`	/ button	/ (limited to room Temple_02)	

## **Built With**
*  Visual Studio
*  TinyXML2 - Used for save files, commands and rooms.	


## **Cheat Guide**
* 	go mountain
* 	go gems
* 	pickup jade
* 	go entrance
* 	place jade
* 	go door
* 	go door
* 	push skull
* 	go door
* 	bow soldier
* 	go door
* 	go door
* 	go crown
* 	*FINISH


