#pragma once
#include <iostream>
#include <string>
#include "tinyxml2.h"

using namespace tinyxml2;

//Class stores all data regarding one event
class Event
{
private:
	std::string target;// What is the name of the target object
	std::string trigger;// What trigger should it activate SurpriseAttack || OpenChest || OpenDoor 
	std::string description;// Description of event on command 
	int damage; // If the event should damage the player
public:
	Event();
	~Event();
	void initialize(XMLElement * element);//Load xml of events related to action
	void save(XMLElement * element);//Save xml of events related to actions
	void display();
	
	std::string getTarget() { return target; }
	std::string getTrigger() { return trigger; }
	std::string getDescription() { return description; }
	int getDamage() { return damage; }
};

