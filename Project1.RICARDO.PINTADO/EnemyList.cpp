#include "EnemyList.h"



EnemyList::EnemyList()
{
}


EnemyList::~EnemyList()
{
	for (auto enemy : enemies) { delete(enemy); }
}

void EnemyList::initialize(XMLElement * element)
{
	XMLElement * enemyElement = element->FirstChildElement("Enemy");
	if (enemyElement != nullptr)
	{
		//Create an iterator for going over the enemy
		XMLElement* iteratorElement = enemyElement;
		while (iteratorElement != NULL) {

			//Create an enemy
			Enemy* enemy = new Enemy();
			// initialize it with the value of the XML
			enemy->initialize(iteratorElement);
			//Add nemy to list
			addEnemy(enemy);

			//Find next enemy to add if there is
			iteratorElement = iteratorElement->NextSiblingElement("Enemy");
		}
	}
}

void EnemyList::save(XMLElement * element)
{
	for (auto enemy : enemies)
	{
		XMLElement* enemyElement = element->GetDocument()->NewElement("Enemy");
		enemy->save(enemyElement);
		element->LinkEndChild(enemyElement);
	}
}

void EnemyList::addEnemy(Enemy * enemy)
{
	enemies.push_back(enemy);
}

void EnemyList::removeEnemy(Enemy * enemy)
{
	enemies.remove(enemy);
	delete(enemy);
}

Enemy * EnemyList::FindEnemyByName(std::string & name)
{
	for (auto enemy : enemies)
	{
		if (name == enemy->getName())
		{
			return enemy;
		}
	}
	return nullptr;
}
