#include "Chest.h"



Chest::Chest(): key(""), isOpen(false)
{
}

Chest::~Chest()
{
}

void Chest::initialize(XMLElement* element)
{
	///////////////////////////CHEST//////////////////////////////////////
	// Initialize Chest isOpen
	XMLElement * isOpenElement = element->FirstChildElement("isOpen");
	if (isOpenElement != nullptr)
	{
		//Get isOpen
		isOpenElement->QueryBoolText(&isOpen);
	}

	//Initialize Chest name
	XMLElement * nameElement = element->FirstChildElement("name");
	if (nameElement != nullptr)
	{
		//Get description
		name = nameElement->GetText();
	}

	// Initialize Chest Key
	XMLElement * keyElement = element->FirstChildElement("key");
	if (keyElement != nullptr)
	{
		//Get key
		key = keyElement->GetText();
	}

	//Initialize Chest Description
	XMLElement * descriptionElement = element->FirstChildElement("description");
	if (descriptionElement != nullptr)
	{
		//Get description
		description= descriptionElement->GetText();
	}

	///////////////////////////ITEMS LIST//////////////////////////////////////
	XMLElement * itemsElement = element->FirstChildElement("Items");
	if (itemsElement != nullptr)
	{
		itemList.initialize(itemsElement);
	}

	///////////////////////////WEAPONS LIST//////////////////////////////////////
	XMLElement * weaponsElement = element->FirstChildElement("Weapons");
	if (weaponsElement != nullptr)
	{
		weaponList.initialize(weaponsElement);
	}

}

void Chest::save(XMLElement * element)
{
	//Save isOpen
	XMLElement* isOpenElement = element->GetDocument()->NewElement("isOpen");
	isOpenElement->SetText(isOpen);
	element->LinkEndChild(isOpenElement);

	//Save name
	XMLElement* nameElement = element->GetDocument()->NewElement("name");
	nameElement->SetText(this->name.c_str());
	element->LinkEndChild(nameElement);

	//Save description
	XMLElement* descriptionElement = element->GetDocument()->NewElement("description");
	descriptionElement->SetText(this->description.c_str());
	element->LinkEndChild(descriptionElement);

	//Save key
	XMLElement* keyElement = element->GetDocument()->NewElement("key");
	keyElement->SetText(this->key.c_str());
	element->LinkEndChild(keyElement);

	//Save all Items in Room
	XMLElement* itemsElement = element->GetDocument()->NewElement("Items");
	itemList.save(itemsElement);
	element->LinkEndChild(itemsElement);

	//Save all Weapons in Room
	XMLElement* weaponsElement = element->GetDocument()->NewElement("Weapons");
	weaponList.save(weaponsElement);
	element->LinkEndChild(weaponsElement);
}

void Chest::display()
{
	std::cout << "////CHEST////" << std::endl;
	std::cout << "Name: " << name << std::endl;
	std::cout << "Description: " << description << std::endl << std::endl;
	if (isOpen)
	{
		std::cout << "ITEMS IN CHEST: " << std::endl << std::endl;
		itemList.display();
		weaponList.display();
	}
	std::cout<< std::endl << std::endl;
}


void Chest::OpenChest(const char * key)
{
	if (key == this->key)
	{
		isOpen = true;
		std::cout << "The chest opens: " << std::endl;
		display();
	}
	else
	{
		std::cout << "Incorrect key" << std::endl;
	}
}
