#pragma once
#include "itemList.h"
#include "WeaponList.h"
#include <list>

// Chess class for storing lists of items and weapons inside a chest
class Chest
{
private:
	bool isOpen;
	std::string name;
	std::string description;
	std::string key;
	
public:
	ItemList itemList;
	WeaponList weaponList;

	Chest();
	~Chest();
	void initialize(XMLElement* element);// Loads data from xml to the chest
	void save(XMLElement* element);// Save data to an XML
	void display();// Displays the information regarding the chest, if is open is true displays information of the items in it
	void OpenChest(const char* key);// changes isOpen bool to true if the passed in key mathches the chests key
	std::string getName() { return name; }
	bool getIsOpen() { return isOpen; }
};

