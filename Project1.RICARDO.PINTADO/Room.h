#pragma once
#include "ItemList.h"
#include "WeaponList.h"
#include "ChestList.h"
#include "EnemyList.h"
#include "CommandList.h"

#include <list>
#include <map>

// This class represents a room in the game, it has a name and description and it contains a list of: chests, weapons, items, enemies and commands. Like any normal room.
class Room
{
private:
	std::string name;
	bool isActive;
	std::string description;

public:
	//Objects in a Room
	ItemList itemList;
	WeaponList weaponList;
	ChestList chestList;
	EnemyList enemyList;
	CommandList commandList;

	Room();
	~Room();
	void initialize(XMLElement * element);
	void save(std::string &fp);
	void display();// Prints all items, enemies, weapons, treasure chests in room

	bool getIsActive() { return isActive; }// Returns true if the room is accesible
	std::string& getName() { return name; }// Returns the rooms name
};

