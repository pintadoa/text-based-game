#pragma once
#include <stdio.h>
#include <string>
#include <iostream>
#include "tinyxml2.h"

using namespace tinyxml2;

//Class stores all data regarding one item
class Item
{
protected:
	std::string name;
	std::string description;
	int effect; //Used if item adds to health
public:
	Item();
	virtual ~Item();
	void initialize(XMLElement* element);//Load Item from XMLelement
	void save(XMLElement* element);// Saves Item
	void display();//Displays information about item
	
	//Setters
	void setName(const char *name) { this->name = name; }
	void setDescription(const char *description) { this->description = description; }

	//Getters
	std::string getName() { return name; }
	std::string getDescription() { return name; }
	int getEffect() { return effect; }
};

