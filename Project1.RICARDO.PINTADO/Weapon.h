#pragma once
#include <iostream>
#include <string>
#include "tinyxml2.h"
#include "Item.h"

using namespace tinyxml2;

//Class stores all data regarding one weapon
class Weapon
{
	std::string name;
	std::string description;
	int damage;

public:
	Weapon();
	~Weapon();
	void initialize(XMLElement* element);//Load Weapon from XMLelement
	void save(XMLElement* element);// Save Weapon from XMLelement
	void display();//Displays information about weapon

	//Getters
	int getDamage() { return damage; }
	std::string getName() { return name; }
};

