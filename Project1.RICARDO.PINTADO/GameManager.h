#pragma once
#include <iostream>
#include <sstream>
#include <algorithm>
#include <ctime>
#include <random>
#include <vector>

#include "Room.h"
#include "Player.h"
#include "Analytics.h"

enum class EActionStatus
{
	Invalid_action,
	OK
};

//Class in charge of managing the conection between the room and the player

class GameManager
{

private:
	std::string fpStart = "../Assets/RoomStart.xml";
	std::string fpSaveLevel = "../Assets/SaveLevel.xml";
	std::string fpSavePlayer = "../Assets/SavePlayer.xml";
	std::string fp = "../Assets/";

	// Generating random number for critical hit in battle
	std::mt19937 generator;

	// Actions map with function pointers to the actions
	std::map<std::string, bool(GameManager::*)(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate)> actionsMap;

	// Map containing all the rooms that can be accessed
	std::map<std::string, bool> activeRooms;

	// Map containing all the rooms the player has traveled through
	std::map<std::string, Room*> createdRooms;

	Analytics * analytics = new Analytics();// Instance in charge of gameplay analytics
	Room *currentRoom = new Room(); // The Current Room the player is in
	Player *player = new Player();// Player instance
public:
	GameManager();
	~GameManager();
	bool initialize(std::string fp);// Load data from XML
	Room* LoadRoomByName(const char* room); // Loads the room by the name of the XML file
	void GameLoop(); // The game Loop
	void battle(Enemy *enemy);

	// Splits user command into action and predicate
	void SplitUserCommand(std::string &command, std::string & action, std::vector<std::string> &predicate);
	// Uses the split command and predicate calling the Actions and Triggers map
	EActionStatus HandleInput(std::string & action, std::vector<std::string> &predicate);

	//Trigger Function Pointers 
	bool HandleTrigger(Command* searchCommand, std::string &trigger, std::string &key);

	//Actions Function Pointers
	bool pickup(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);
	bool place(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);
	bool equip(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);
	bool go(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);
	bool help(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);
	bool look(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);
	bool save(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);
	bool load(Command* searchCommand, std::vector<std::string> &predicate, std::string &completePredicate);

	//Pickup Items and Weapons
	bool pickupItemFromTarget(Item *selectedItem, Chest *targetChest);// Removes Item from target chest or Room and adds it to player inventory
	bool pickupWeaponFromTarget(Weapon *selectedItem, Chest *targetChest);// Removes Weapon from target chest or Room and adds it to player inventory

	//Drop on Death
	void dropOnDeathInventory();//Leaves items where player died
	void Reset();// Prepares game for another run after the player dies
};

