#include "Enemy.h"



Enemy::Enemy(): name(""), health(1), attack(0)
{
}


Enemy::~Enemy()
{
}

void Enemy::initialize(XMLElement * element)
{
	XMLElement * nameElement = element->FirstChildElement("name");
	if (nameElement == nullptr)
	{
		std::cout << "ERROR LOADING: enemy name not found" << std::endl;
		return;
	}

	//Get name
	name = nameElement->GetText();

	XMLElement * healthElement = element->FirstChildElement("health");
	if (healthElement == nullptr)
	{
		std::cout << "ERROR LOADING: enemy damage not found" << std::endl;
		return;
	}

	//Get damage
	healthElement->QueryIntText(&health);


	XMLElement * damageElement = element->FirstChildElement("attack");
	if (damageElement == nullptr)
	{
		std::cout << "ERROR LOADING: enemy attack not found" << std::endl;
		return;
	}

	//Get damage
	damageElement->QueryIntText(&attack);
}

void Enemy::save(XMLElement * element)
{
	//Save name
	XMLElement* nameElement = element->GetDocument()->NewElement("name");
	nameElement->SetText(this->name.c_str());
	element->LinkEndChild(nameElement);

	//Save health
	XMLElement* healthElement = element->GetDocument()->NewElement("health");
	healthElement->SetText(health);
	element->LinkEndChild(healthElement);

	//Save attack
	XMLElement* attackElement = element->GetDocument()->NewElement("attack");
	attackElement->SetText(attack);
	element->LinkEndChild(attackElement);
}

void Enemy::display()
{
	std::cout << "////ENEMY////"<< std::endl;
	std::cout << "Name: " << name << std::endl;
	std::cout << "Health: " << health << std::endl;
	std::cout << "Attack Damage: " << attack << std::endl << std::endl;
}

void Enemy::takeDamage(int damage)
{
	health -= damage;
	
	//Logout the damage info
	std::cout << name << " takes " << damage << "HP of damage" << std::endl;
	std::cout << "Remaining HP " << health << std::endl << std::endl;
}

bool Enemy::isEnemyDead()
{
	if (health <= 0)
	{
		return true;
	}
	return false;
}
