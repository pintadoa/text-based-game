#pragma once
#include "Item.h"
#include <algorithm>
#include <list>

//Class containing a dynamic list of items
class ItemList
{
public:
	std::list <Item*> items;//List Containing Items 

	ItemList();
	~ItemList();
	void initialize(XMLElement * element);//Loading all items from XML
	void save(XMLElement * element);//Saving all items for XML
	void addItem(Item* item);
	void removeItem(Item* item);
	void display();//Loops through all members printing their information
	Item* FindItemByName(std::string& name);//Recieves a string and looks for the Item by Name
};

