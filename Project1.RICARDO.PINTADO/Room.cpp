#include "Room.h"



Room::Room():name(""),isActive(false), description(""), itemList(), weaponList(), chestList(), enemyList()
{
}


Room::~Room()
{
}

void Room::initialize(XMLElement * element)
{
		///////////////////////////ROOM//////////////////////////////////////
		// Initialize Room name
		XMLElement * nameElement = element->FirstChildElement("name");
		if (nameElement != nullptr)
		{
			//Get Name
			name = nameElement->GetText();
		}

		// Initialize Room isActive
		XMLElement * isActiveElement = element->FirstChildElement("isActive");
		if (isActiveElement != nullptr)
		{
			//Get is Active
			isActiveElement->QueryBoolText(&isActive);
		}

		// Initialize Room description
		XMLElement * descriptionElement = element->FirstChildElement("description");
		if (descriptionElement != nullptr)
		{
			//Get Name
			description = descriptionElement->GetText();
		}

		///////////////////////////COMMAND LIST//////////////////////////////////////
		XMLElement * commandsElement = element->FirstChildElement("Commands");
		if (commandsElement != nullptr)
		{
			commandList.initialize(commandsElement);
		}

		///////////////////////////ITEMS LIST//////////////////////////////////////
		// Initialize Items in Room
		XMLElement * itemsElement = element->FirstChildElement("Items");
		if (itemsElement != nullptr)
		{
			itemList.initialize(itemsElement);
		}

		///////////////////////////WEAPONS LIST//////////////////////////////////////
		XMLElement * weaponsElement = element->FirstChildElement("Weapons");
		if (weaponsElement != nullptr)
		{
			weaponList.initialize(weaponsElement);
		}

		///////////////////////////CHEST LIST//////////////////////////////////////
		// Initialize Chest in Room
		XMLElement * chestsElement = element->FirstChildElement("Chests");
		if (chestsElement != nullptr)
		{
			chestList.initialize(chestsElement);
		}

		///////////////////////////ENEMY//////////////////////////////////////
		// Initialize Enemy in Room
		XMLElement * enemiesElement = element->FirstChildElement("Enemies");
		if (enemiesElement != nullptr)
		{
			enemyList.initialize(enemiesElement);
		}
}

void Room::save(std::string & fp)
{
	XMLDocument* doc = new XMLDocument();

	XMLElement* roomElement = doc->NewElement("Room");

	//Save Name
	XMLElement* nameElement = roomElement->GetDocument()->NewElement("name");
	nameElement->SetText(name.c_str());
	roomElement->LinkEndChild(nameElement);

	//Save isActive
	XMLElement* isActiveElement = roomElement->GetDocument()->NewElement("isActive");
	isActiveElement->SetText(isActive);
	roomElement->LinkEndChild(isActiveElement);

	//Save Description
	XMLElement* descriptionElement = roomElement->GetDocument()->NewElement("description");
	descriptionElement->SetText(description.c_str());
	roomElement->LinkEndChild(descriptionElement);

	//Save all Commands in Room
	XMLElement* commandsElement = roomElement->GetDocument()->NewElement("Commands");
	commandList.save(commandsElement);
	roomElement->LinkEndChild(commandsElement);

	//Save all Items in Room
	XMLElement* itemsElement = roomElement->GetDocument()->NewElement("Items");
	itemList.save(itemsElement);
	roomElement->LinkEndChild(itemsElement);

	//Save all Weapons in Room
	XMLElement* weaponsElement = roomElement->GetDocument()->NewElement("Weapons");
	weaponList.save(weaponsElement);
	roomElement->LinkEndChild(weaponsElement);

	//Save all Chests in Room
	XMLElement* chestsElement = roomElement->GetDocument()->NewElement("Chests");
	chestList.save(chestsElement);
	roomElement->LinkEndChild(chestsElement);

	//Save all Enemies in Room
	XMLElement* EnemyElement = roomElement->GetDocument()->NewElement("Enemies");
	enemyList.save(EnemyElement);
	roomElement->LinkEndChild(EnemyElement);

	//Insert to doc for saving
	doc->InsertEndChild(roomElement);

	doc->SaveFile(fp.c_str());
	if (doc->Error() != XML_SUCCESS)
	{
		std::cout << "ERROR SAVING: Data Files did not save" << std::endl;
		delete(doc);
		return;
	}
	delete(doc);
}

void Room::display()
{
	
	std::cout << "////ROOM////" << std::endl;
	std::cout << "Name: " << name << std::endl;
	std::cout << "Description: " << description << std::endl << std::endl;

	weaponList.display();
	itemList.display();
	chestList.display();
}



