#include "Command.h"



Command::Command() :action(""), predicate(""), goToRoom("")
{
}

Command::Command(std::string action, std::string predicate, std::string goToRoom, std::string eventDescription) 
	: action(action), predicate(predicate), goToRoom(goToRoom)
{
}

Command::~Command()
{
	delete(gameEvent);
}

void Command::initialize(XMLElement * element)
{

	XMLElement * actionElement = element->FirstChildElement("action");
	if (actionElement == nullptr)
	{
		std::cout << "ERROR LOADING: action not found" << std::endl;
		return;
	}

	//Get action
	action = actionElement->GetText();

	XMLElement * predicateElement = element->FirstChildElement("predicate");
	if (predicateElement != nullptr)
	{
		//Get predicate
		if (predicateElement->GetText() != nullptr) 
		{
			predicate = predicateElement->GetText();
		}
	}

	XMLElement * goToElement = element->FirstChildElement("goTo");
	if (goToElement != nullptr)
	{
		//Get goToRoom
		if (goToElement->GetText() != nullptr)
		{
			goToRoom = goToElement->GetText();
		}
	}

	XMLElement * eventElement = element->FirstChildElement("Event");
	if (eventElement != nullptr)
	{
		gameEvent->initialize(eventElement);
	}
}

void Command::save(XMLElement * element)
{
	//Save name
	XMLElement* actionElement = element->GetDocument()->NewElement("action");
	actionElement->SetText(this->action.c_str());
	element->LinkEndChild(actionElement);

	//Save description
	XMLElement* predicateElement = element->GetDocument()->NewElement("predicate");
	predicateElement->SetText(this->predicate.c_str());
	element->LinkEndChild(predicateElement);

	//Save description
	XMLElement* goToRoomElement = element->GetDocument()->NewElement("goTo");
	goToRoomElement->SetText(this->goToRoom.c_str());
	element->LinkEndChild(goToRoomElement);

	//Save event
	XMLElement* EventElement = element->GetDocument()->NewElement("Event");
	gameEvent->save(EventElement);
	element->LinkEndChild(EventElement);
}

void Command::display()
{
	std::cout << action << " " << predicate << std::endl;
}

