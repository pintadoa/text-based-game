#include "Player.h"



Player::Player(): health(10), attack(3)
{
}

Player::Player(int health, int attack): health(health), attack(attack)
{
}


Player::~Player()
{
}

bool Player::initialize(std::string &fp)
{
	XMLDocument* doc = new XMLDocument();
	doc->LoadFile(fp.c_str());
	if (doc->Error() != XML_SUCCESS)
	{
		delete(doc);
		return false;
	}

	XMLElement * playerElement = doc->FirstChildElement("Player");
	if (playerElement != nullptr)
	{
		// Load Player Health
		XMLElement * healthElement = playerElement->FirstChildElement("health");
		if (healthElement != nullptr)
		{
			//Get Health
			healthElement->QueryIntText(&health);
		}

		// Load Player Attack
		XMLElement * attackElement = playerElement->FirstChildElement("attack");
		if (attackElement != nullptr)
		{
			//Get Attack
			attackElement->QueryIntText(&attack);
		}

		///////////////////////////EQUIPPED WEAPON//////////////////////////////////////
		// Load equipped weapon
		XMLElement * weaponElement = playerElement->FirstChildElement("Weapon");
		if (weaponElement != nullptr)
		{
			equipedWeapon->initialize(weaponElement);
		}

		///////////////////////////ITEMS LIST//////////////////////////////////////
		// Initialize Items in Room
		XMLElement * itemsElement = playerElement->FirstChildElement("Items");
		if (itemsElement != nullptr)
		{
			itemList.initialize(itemsElement);
		}

		///////////////////////////WEAPONS LIST//////////////////////////////////////
		XMLElement * weaponsElement = playerElement->FirstChildElement("Weapons");
		if (weaponsElement != nullptr)
		{
			weaponList.initialize(weaponsElement);
		}
	}

	delete(doc);
	return true;
}

void Player::save(std::string &fp)
{
	XMLDocument* doc = new XMLDocument();

	XMLElement* playerElement = doc->NewElement("Player");

	//Save Health
	XMLElement* healthElement = playerElement->GetDocument()->NewElement("health");
	healthElement->SetText(health);
	playerElement->LinkEndChild(healthElement);

	//Save Attack
	XMLElement* attackElement = playerElement->GetDocument()->NewElement("attack");
	attackElement->SetText(attack);
	playerElement->LinkEndChild(attackElement);

	//Save Equipped Weapon
	XMLElement* equippedWeaponElement = playerElement->GetDocument()->NewElement("equippedWeapon");
	equipedWeapon->save(equippedWeaponElement);
	playerElement->LinkEndChild(equippedWeaponElement);

	//Save all Items in Inventory
	XMLElement* itemsElement = playerElement->GetDocument()->NewElement("Items");
	itemList.save(itemsElement);
	playerElement->LinkEndChild(itemsElement);

	//Save all Weapons in Inventory
	XMLElement* weaponsElement = playerElement->GetDocument()->NewElement("Weapons");
	weaponList.save(weaponsElement);
	playerElement->LinkEndChild(weaponsElement);

	//Insert to doc for saving
	doc->InsertEndChild(playerElement);

	doc->SaveFile(fp.c_str());
	if (doc->Error() != XML_SUCCESS)
	{
		std::cout << "ERROR SAVING: Data Files did not save" << std::endl;
		delete(doc);
		return;
	}
	delete(doc);
}

bool Player::pickUpItem(Item * selectedItem)
{
	if (selectedItem != nullptr)
	{
		itemList.addItem(selectedItem);
		return true;
	}

	return false;
}

bool Player::pickUpWeapon(Weapon * selectedWeapon)
{
	if (selectedWeapon != nullptr)
	{
		weaponList.addWeapon(selectedWeapon);
		return true;
	}

	return false;
}

bool Player::placeItem(std::string &name)
{
	Item* selectedItem = itemList.FindItemByName(name);
	if (selectedItem != nullptr)
	{
		itemList.removeItem(selectedItem);
		delete(selectedItem);
		return true;
	}

	return false;
}

void Player::takeDamage(int damage, Analytics *analytics)
{
	health -= damage;
	std::cout << "You take " << damage << "HP of damage" <<std::endl;
	std::cout << "Remaining HP " << health << std::endl << std::endl;
	analytics->increaseNumDamage();
}

bool Player::isPlayerDead()
{
	if (health <= 0) 
	{
		return true;
	}
	return false;
}

void Player::addToHealth(int effect)
{
	health += effect;
}

bool Player::equipWeapon(std::string & name)
{
	Weapon* selectedWeapon = weaponList.FindWeaponByName(name);
	if (selectedWeapon != nullptr)
	{
		if (equipedWeapon != nullptr)
		{
			//Since player changes revert to default attack value
			attack -= equipedWeapon->getDamage();
		}
		attack += selectedWeapon->getDamage(); // Add to player Attack
		equipedWeapon = selectedWeapon; // Select new weapon
		std::cout << "You equip: " << std::endl;
		equipedWeapon->display();

		return true;
	}
	return false;
}

