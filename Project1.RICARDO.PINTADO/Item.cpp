#include "Item.h"



Item::Item() : name(""), description(""), effect(0)
{
}


Item::~Item()
{
}

void Item::initialize(XMLElement * element)
{
	XMLElement * nameElement = element->FirstChildElement("name");
	if (nameElement == nullptr)
	{
		std::cout << "ERROR LOADING: item name not found" << std::endl;
		return;
	}

	//Get name
	name = nameElement->GetText();

	XMLElement * descriptionElement = element->FirstChildElement("description");
	if (descriptionElement == nullptr)
	{
		std::cout << "ERROR LOADING: item description not found" << std::endl;
		return;
	}

	//Get description
	description = descriptionElement->GetText();

	XMLElement * effectElement = element->FirstChildElement("effect");
	if (effectElement != nullptr)
	{
		//Get damage
		effectElement->QueryIntText(&effect);
	}
}

void Item::save(XMLElement * element)
{
	//Save name
	XMLElement* nameElement = element->GetDocument()->NewElement("name");
	nameElement->SetText(this->name.c_str());
	element->LinkEndChild(nameElement);

	//Save description
	XMLElement* descriptionElement = element->GetDocument()->NewElement("description");
	descriptionElement->SetText(this->description.c_str());
	element->LinkEndChild(descriptionElement);

	//Save effect
	XMLElement* effectElement = element->GetDocument()->NewElement("effect");
	effectElement->SetText(effect);
	element->LinkEndChild(effectElement);
}

void Item::display()
{
	std::cout << "////ITEM////" << std::endl;
	std::cout << "Name: " << name << std::endl;
	std::cout << "Description: " << description << std::endl;
	if (effect > 0)
	{
		std::cout << "Adds: " << effect << "HP to player" << std::endl;
	}
	std::cout << std::endl;
}
