#include "ChestList.h"



ChestList::ChestList()
{
}


ChestList::~ChestList()
{
	for (auto chest : chests) { delete(chest); }
}

void ChestList::initialize(XMLElement * element)
{
	XMLElement * chestElement = element->FirstChildElement("Chest");
	if (chestElement != nullptr)
	{
		//Create an iterator for going over the Chests
		XMLElement* iteratorElement = chestElement;
		while (iteratorElement != NULL) {

			//Create an chest
			Chest* chest = new Chest();
			// initialize it with the value of the XML
			chest->initialize(iteratorElement);
			//Add chest to list
			addChest(chest);

			//Find next chest to add if there is
			iteratorElement = iteratorElement->NextSiblingElement("Chest");
		}
	}
}

void ChestList::save(XMLElement * element)
{
	for (auto chest : chests)
	{
		XMLElement* chestElement = element->GetDocument()->NewElement("Chest");
		chest->save(chestElement);
		element->LinkEndChild(chestElement);
	}
}

void ChestList::addChest(Chest * chest)
{
	chests.push_back(chest);
}

void ChestList::removeChest(Chest * chest)
{
	chests.remove(chest);
}

void ChestList::display()
{
	if (chests.size() > 0)
	{
		for (auto member : chests)
		{
			member->display();
		}
	}
}

Chest* ChestList::FindChestByName(std::string& name)
{
	for (auto chest : chests)
	{
		if (name.c_str() == chest->getName())
		{
			return chest;
		}
	}
	return nullptr;
}