#pragma once
#include "ItemList.h"
#include "WeaponList.h"
#include "Analytics.h"

//CLass in charge of handling all the players stuff. Weapons, items, health, damage, etc.
class Player
{
private:

	int health; // health a player has 
	int attack; // attack damage
	Weapon* equipedWeapon = new Weapon();// Equipped weapon
	
public:
	ItemList itemList;
	WeaponList weaponList;
	Player();
	Player(int health, int attack);
	~Player();
	bool initialize(std::string &fp);
	void save(std::string &fp);
	bool pickUpItem(Item* selectedItem);// Picks up and item and places it in the players item list
	bool pickUpWeapon(Weapon * selectedWeapon);// Picks up and weapon and places it in the players weapon list 
	bool placeItem(std::string & name);// Checks to see if player has said item and then places it, the item is also deleted from the playes list
	void takeDamage(int damage, Analytics *analytics); // Decreases heakth of the player by the damage taken
	bool isPlayerDead(); // Checks if player life is less or equal to cero
	void addToHealth(int effect);// Adds the effect to the players health
	bool equipWeapon(std::string &name);// Equips a weapon to a player

	int playerGetAttack() { return attack; }
	
};