#include "ItemList.h"



ItemList::ItemList()
{
}


ItemList::~ItemList()
{
	for (auto item : items) { delete(item); }
}

void ItemList::initialize(XMLElement * element)
{
	//Add all Items
	XMLElement * itemElement = element->FirstChildElement("Item");
	if (itemElement != nullptr)
	{
		//Create an iterator for going over the items
		XMLElement* iteratorElement = itemElement;
		while (iteratorElement != NULL) {

			//Create an item
			Item* item = new Item();
			// initialize it with the value of the XML
			item->initialize(iteratorElement);
			//Add item to list
			addItem(item);

			//Find next item to add if there is
			iteratorElement = iteratorElement->NextSiblingElement("Item");
		}
	}
}

void ItemList::save(XMLElement * element)
{
	for (auto item : items)
	{
		XMLElement* itemElement = element->GetDocument()->NewElement("Item");
		item->save(itemElement);
		element->LinkEndChild(itemElement);
	}
}

void ItemList::addItem(Item * item)
{
	items.push_back(item);
}

void ItemList::removeItem(Item * item)
{
	items.remove(item);
}

void ItemList::display()
{
	if (items.size() > 0)
	{
		for (auto member : items)
		{
			member->display();
		}
	}
}

Item* ItemList::FindItemByName(std::string& name)
{
	for (auto item : items)
	{
		std::string lower = item->getName();
		std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
		if (name == lower)
		{
			return item;
		}
	}
	return nullptr;
}