#pragma once
#include <iostream>
#include <string>
#include "tinyxml2.h"
#include "Event.h"

using namespace tinyxml2;

// Command class for storing commands and their events
class Command
{
private:
	std::string action;// Go, swim, run, hide, attack, pick up, etc.
	std::string predicate;// Words after the indicated action 
	std::string goToRoom;// Name of the room the command will take if none ""
	
public:
	Event *gameEvent = new Event();
	Command();
	Command(std::string action, std::string predicate, std::string goToRoom, std::string eventDescription);
	~Command();
	void initialize(XMLElement * element);//Load xml of commands related to action
	void save(XMLElement * element);//Save xml of commands 
	void display();

	std::string getAction() { return action; }
	std::string getPredicate() { return predicate; }
	std::string getGoToRoom() { return goToRoom; }
	
};

