#include "WeaponList.h"



WeaponList::WeaponList()
{
}


WeaponList::~WeaponList()
{
	for (auto weapon : weapons) { delete(weapon); }
}

void WeaponList::initialize(XMLElement * element)
{
	XMLElement * weaponElement = element->FirstChildElement("Weapon");
	if (weaponElement != nullptr)
	{
		//Create an iterator for going over the weapons
		XMLElement* iteratorElement = weaponElement;
		while (iteratorElement != NULL) {

			//Create an item
			Weapon* weapon = new Weapon();
			// initialize it with the value of the XML
			weapon->initialize(iteratorElement);
			//Add item to list
			addWeapon(weapon);

			//Find next item to add if there is
			iteratorElement = iteratorElement->NextSiblingElement("Weapon");
		}
	}
}

void WeaponList::save(XMLElement * element)
{
	for (auto weapon : weapons)
	{
		XMLElement* weaponElement = element->GetDocument()->NewElement("Weapon");
		weapon->save(weaponElement);
		element->LinkEndChild(weaponElement);
	}
}

void WeaponList::addWeapon(Weapon * weapon)
{
	weapons.push_back(weapon);
}

void WeaponList::removeWeapon(Weapon * weapon)
{
	weapons.remove(weapon);
}

void WeaponList::display()
{
	if (weapons.size() > 0)
	{
		for (auto member : weapons)
		{
			member->display();
		}
	}
}

Weapon* WeaponList::FindWeaponByName(std::string& name)
{
	for (auto weapon : weapons)
	{
		std::string lower = weapon->getName();
		std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
		if (name == lower)
		{
			return weapon;
		}
	}
	return nullptr;
}
