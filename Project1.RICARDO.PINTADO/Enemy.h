#pragma once
#include <string>
#include <iostream>
#include "tinyxml2.h"

using namespace tinyxml2;

//Enemy class stores all data regarding one enemy
class Enemy
{
private:
	std::string name;
	int health;
	int attack;
public:
	Enemy();
	~Enemy();
	void initialize(XMLElement* element);//Load Enemy from XMLelement
	void save(XMLElement* element);//Save Enemy to XMLelement
	void display();//Displays information about enemy
	void takeDamage(int damage);//Subtracts the amount of damage from enemies current health
	bool isEnemyDead();//Checks if health is less or equal to cero returns: true, false

	std::string getName() { return name; }
	int getAtack() { return attack; }
};

