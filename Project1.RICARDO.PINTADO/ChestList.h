#pragma once
#include "Chest.h"
#include<list>

// Class containing an dinamic list of chests 
class ChestList
{
public:
	std::list <Chest*> chests;// List Containing Chests 

	ChestList();
	~ChestList();
	void initialize(XMLElement * element);// Loading all chests from XML
	void save(XMLElement * element);// Save all chests to an XML
	void addChest(Chest* chest);
	void removeChest(Chest* chest);
	void display();// Displays the information regarding the chest and the items it contains if open
	Chest* FindChestByName(std::string& name);// Looks for the chest with the same name as the passed in string 
};

