#pragma once
#include <iostream>
#include "tinyxml2.h"

using namespace tinyxml2;

// Analytics class is in charge of gathering all the data for the analytics.
class Analytics
{
private:
	int numActions;
	int numPickup;
	int numPlays;
	int numDamage;
	std::string fpAnalytics = "../Assets/Analytics.xml";

public:
	Analytics();
	~Analytics();
	void initialize();
	void save();
	void increaseNumActions();// Increases NumActions by 1
	void increaseNumPickups();// Increases NumPickups by 1
	void increaseNumPlays();// Increases NumPlays by 1
	void increaseNumDamage();// Increases NumDamage by 1
};

