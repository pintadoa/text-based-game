#include "Event.h"

Event::Event(): description(""), damage(0)
{
}


Event::~Event()
{
}

void Event::initialize(XMLElement * element)
{

	// Assign Target
	XMLElement * triggerElement = element->FirstChildElement("trigger");
	if (triggerElement != nullptr)
	{
		//Get Description
		if (triggerElement->GetText() != nullptr)
		{
			trigger = triggerElement->GetText();
		}
	}

	// Assign Target
	XMLElement * targetElement = element->FirstChildElement("target");
	if (targetElement != nullptr)
	{
		//Get Description
		if (targetElement->GetText() != nullptr)
		{
			target = targetElement->GetText();
		}
	}

	// Assign Description
	XMLElement * descriptionElement = element->FirstChildElement("description");
	if (descriptionElement != nullptr)
	{
		//Get Description
		if (descriptionElement->GetText() != nullptr)
		{
			description = descriptionElement->GetText();
		}
	}

	XMLElement * damageElement = element->FirstChildElement("damage");
	if (damageElement != nullptr)
	{
		damageElement->QueryIntText(&damage);
	}
}

void Event::save(XMLElement * element)
{
	//Save target
	XMLElement* targetElement = element->GetDocument()->NewElement("target");
	targetElement->SetText(this->target.c_str());
	element->LinkEndChild(targetElement);

	//Save trigger
	XMLElement* triggerElement = element->GetDocument()->NewElement("trigger");
	triggerElement->SetText(this->trigger.c_str());
	element->LinkEndChild(triggerElement);

	//Save description
	XMLElement* descriptionElement = element->GetDocument()->NewElement("description");
	descriptionElement->SetText(this->description.c_str());
	element->LinkEndChild(descriptionElement);

	//Save damage
	XMLElement* damageElement = element->GetDocument()->NewElement("damage");
	damageElement->SetText(damage);
	element->LinkEndChild(damageElement);
}

void Event::display()
{
	if (description != "")
	{
		std::cout << "////EVENT////" << std::endl;
		std::cout << "Description: " << description << std::endl;
		if (damage > 0)
		{
			std::cout << "Damage: " << damage << std::endl;
		}
		std::cout << std::endl;
	}
}
