#include "CommandList.h"



CommandList::CommandList()
{
}


CommandList::~CommandList()
{
	for (auto command : commands) { delete(command); }
}

void CommandList::initialize(XMLElement * element)
{
	//Add all commands avaiable to the room
	XMLElement * commandElement = element->FirstChildElement("Command");
	if (commandElement != nullptr)
	{
		//Create an iterator for going over the items
		XMLElement* iteratorElement = commandElement;
		while (iteratorElement != NULL) {

			//Create an item
			Command* command = new Command();
			// initialize it with the value of the XML
			command->initialize(iteratorElement);
			//Add item to list
			addCommand(command);

			//Find next item to add if there is
			iteratorElement = iteratorElement->NextSiblingElement("Command");
		}
	}
	
	for (auto publicCommand : publicCommands)
	{
		Command* command = new Command(publicCommand, "", "", "");
		addCommand(command);
	}
}

void CommandList::save(XMLElement * element)
{
	for (auto command : commands)
	{
		std::string action = command->getAction();
		if (std::find(std::begin(publicCommands), std::end(publicCommands), action) == std::end(publicCommands))
		{
			XMLElement* commandElement = element->GetDocument()->NewElement("Command");
			command->save(commandElement);
			element->LinkEndChild(commandElement);
		}
	}
}

void CommandList::addCommand(Command * command)
{
	commands.push_back(command);
}

void CommandList::removeCommand(Command * command)
{
	commands.remove(command);
	delete(command);
}

void CommandList::display()
{
	if (commands.size() > 0)
	{
		std::cout << "////COMMANDS////" << std::endl;
		for (auto member : commands) 
		{
			member->display();
		}
		std::cout << std::endl << std::endl;
	}
}

Command * CommandList::SearchByAction(std::string &action, std::string & predicate)
{
	for (auto command : commands)
	{
		std::string loweraction = command->getAction();
		std::transform(loweraction.begin(), loweraction.end(), loweraction.begin(), ::tolower);
		if (loweraction == action)
		{
			if (std::find(std::begin(publicCommands), std::end(publicCommands), action) != std::end(publicCommands))
			{
				return command;
			}
			else
			{
				std::string lower = command->getPredicate();
				std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
				if (lower == predicate)
				{
					return command;
				}
			}
		}
	}
	return nullptr;
}
