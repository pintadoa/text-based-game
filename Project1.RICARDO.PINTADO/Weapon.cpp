#include "Weapon.h"



Weapon::Weapon() : name(""), description(""), damage(0)
{
}


Weapon::~Weapon()
{
}

void Weapon::initialize(XMLElement * element)
{
	XMLElement * nameElement = element->FirstChildElement("name");
	if (nameElement == nullptr)
	{
		std::cout << "ERROR LOADING: item name not found" << std::endl;
		return;
	}
	//Get name
	name = nameElement->GetText();

	XMLElement * descriptionElement = element->FirstChildElement("description");
	if (descriptionElement == nullptr)
	{
		std::cout << "ERROR LOADING: item description not found" << std::endl;
		return;
	}
	description = descriptionElement->GetText();


	XMLElement * damageElement = element->FirstChildElement("damage");
	if (damageElement == nullptr)
	{
		std::cout << "ERROR LOADING: weapon damage not found" << std::endl;
		return;
	}

	//Get damage
	damageElement->QueryIntText(&damage);
}

void Weapon::save(XMLElement * element)
{
	//Save name
	XMLElement* nameElement = element->GetDocument()->NewElement("name");
	nameElement->SetText(this->name.c_str());
	element->LinkEndChild(nameElement);

	//Save description
	XMLElement* descriptionElement = element->GetDocument()->NewElement("description");
	descriptionElement->SetText(this->description.c_str());
	element->LinkEndChild(descriptionElement);

	//Save effect
	XMLElement* damageElement = element->GetDocument()->NewElement("damage");
	damageElement->SetText(damage);
	element->LinkEndChild(damageElement);
}


void Weapon::display()
{
	std::cout << "////WEAPON////" << std::endl;
	std::cout << "Name: " << name << std::endl;
	std::cout << "Description: " << description << std::endl;
	std::cout << "Attack Damage: " << damage << std::endl << std::endl;
}
