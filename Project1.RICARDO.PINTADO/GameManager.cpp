#include "GameManager.h"





GameManager::GameManager()
{
	generator.seed(std::time(0));
	analytics->initialize();

	//Actions map
	actionsMap["pickup"] = &GameManager::pickup;
	actionsMap["place"] = &GameManager::place;
	actionsMap["equip"] = &GameManager::equip;
	actionsMap["go"] = &GameManager::go;
	actionsMap["push"] = &GameManager::go;
	actionsMap["run"] = &GameManager::go;
	actionsMap["help"] = &GameManager::help;
	actionsMap["look"] = &GameManager::look;
	actionsMap["save"] = &GameManager::save;
	actionsMap["load"] = &GameManager::load;
}


GameManager::~GameManager()
{	
	delete(player);
	delete(analytics);
	for (auto room : createdRooms)
	{
		if (room.second->getName() != currentRoom->getName())
		{
			delete(room.second);
			room.second = nullptr;
		}
	}
	createdRooms.clear();
	delete(currentRoom);
}

bool GameManager::initialize(std::string fp)
{
	XMLDocument* doc = new XMLDocument();
	doc->LoadFile(fp.c_str());
	if (doc->Error() != XML_SUCCESS)
	{
		delete(doc);
		return false;
	}

	XMLElement * roomElement = doc->FirstChildElement("Room");
	if (roomElement != nullptr)
	{
		currentRoom->initialize(roomElement);
	}
	createdRooms[currentRoom->getName()] = currentRoom;
	activeRooms[currentRoom->getName()] = true;
	currentRoom->display();

	delete(doc);
	return true;
}

Room* GameManager::LoadRoomByName(const char * room)
{
	// Check if room has already been loaded
	if (createdRooms.count(room) != 0)
	{
		return createdRooms[room];
	}
	else
	{
		//Load the room if not in createdRooms map nad add it to the map
		std::string fpSelectedRoom = fp + room + ".xml";
		XMLDocument* doc = new XMLDocument();
		doc->LoadFile(fpSelectedRoom.c_str());
		if (doc->Error() != XML_SUCCESS)
		{
			std::cout << "ERROR LOADING: Data File " << fpSelectedRoom << " is not loaded" << std::endl;
			delete(doc);
			return nullptr;
		}

		XMLElement * roomElement = doc->FirstChildElement("Room");
		if (roomElement != nullptr)
		{
			// Gets the Next Room element and returns it 
			Room* nextRoom = new Room();
			nextRoom->initialize(roomElement);


			//Adds the current room to the map of active rooms if it does not exist
			if (activeRooms.count(room) == 0)
			{
				activeRooms[room] = nextRoom->getIsActive();
			}

			delete(doc);
			createdRooms[currentRoom->getName()] = currentRoom;
			return nextRoom;
		}
		return nullptr;
		delete(doc);
	}
}

void GameManager::GameLoop()
{
	std::string  userAction = "";
	std::vector<std::string> userPredicate;

	std::string userCommand;

	if (!this->initialize(fpSaveLevel) || !player->initialize(fpSavePlayer))// Initialize the saved game where the player left it if theres a file
	{
		system("CLS");
		this->initialize(fpStart);// Initialize the game in the beginning
	}
	else
	{
		analytics->increaseNumPlays();//Increase the number of plays each time player opens 
	}
	
	while (userAction != "exit")
	{
		while (!player->isPlayerDead() && userAction != "exit" && currentRoom->getName() != "EndRoom")
		{
			std::cout << "What do you want to do? ";
			std::getline(std::cin, userCommand);
			if (userCommand != "") 
			{
				std::cout << std::endl;

				SplitUserCommand(userCommand, userAction, userPredicate);

				switch (HandleInput(userAction, userPredicate))
				{
				case EActionStatus::Invalid_action:
					std::cout << "Please enter a valid action." << std::endl << std::endl;
					break;
				default:
					analytics->increaseNumActions();
					break;
				}
				userPredicate.clear();
			}
			analytics->save();//Save the current analytics data
		}

		//Reset game for player death or win
		if (userAction != "exit")
		{
			if (currentRoom->getName() != "EndRoom")
			{
				std::cout << "You are dead!!!" << std::endl << std::endl << std::endl;
			}
			system("pause");
			Reset();
		}	
	}
}

void GameManager::battle(Enemy * enemy)
{
	std::uniform_int_distribution<uint32_t> criticalRandom(1, 3);
	int criticalhit = 1;

	while ((!player->isPlayerDead()) && (!enemy->isEnemyDead()))// Battle while player and enemy are alive
	{
		system("pause");
		std::cout << "////BATTLE LOG////" << std::endl << std::endl;
		if (criticalRandom(generator) == 1)// Check if random number is 1 then the player does double damage (critical)
		{
			criticalhit = 2;
			std::cout << "You land a Critical Strike" << std::endl;
		}
		else
		{
			criticalhit = 1;
		}
		enemy->takeDamage(player->playerGetAttack() * criticalhit);

		if (!enemy->isEnemyDead())// Before the enemy attacks check if he is alive
		{
			player->takeDamage(enemy->getAtack(),analytics);
		}
	}

	if (enemy->isEnemyDead())// If the enemy is dead display info to player and delete him
	{
		std::cout << "Enemy: " << enemy->getName() << " is dead!!!" << std::endl << std::endl << std::endl;
		currentRoom->enemyList.removeEnemy(enemy);
	}
}

void GameManager::SplitUserCommand(std::string & command, std::string & action, std::vector<std::string> &predicate)
{
	std::vector <std::string> tokens;
	std::transform(command.begin(), command.end(), command.begin(), ::tolower);//Change command to lower case
	std::stringstream check(command);
	std::string word;

	while (std::getline(check, word, ' '))
	{
		if (word != "")
		{
			tokens.push_back(word);
		}
	}

	action = tokens[0]; 
	for (int i = 1; i < tokens.size(); i++)
	{
		predicate.push_back(tokens[i]);
	}
}

EActionStatus GameManager::HandleInput(std::string & action, std::vector<std::string> &predicate)
{
	std::string completePredicate = "";// Contains the rest of the sentece after user action
	if (predicate.size() > 0)
	{
		for (auto word : predicate)
		{
			completePredicate += word + " ";
		}
		completePredicate.pop_back();
	}

	Command* searchCommand;
	// Search for the action with the nouns returns a pointer of type Command
	searchCommand = currentRoom->commandList.SearchByAction(action, completePredicate);

	if (searchCommand != nullptr)// If the command is valid
	{
		std::string trigger = searchCommand->gameEvent->getTrigger();// Search if the command has a trigger
		HandleTrigger(searchCommand, trigger, completePredicate);// Handle the tigger if there is
		if (actionsMap.find(action) != actionsMap.end()) // Check if the action is in the actions map
		{
			if (!(this->*(actionsMap[action]))(searchCommand, predicate, completePredicate))// Do the action 
			{
				return (EActionStatus::Invalid_action);
			}
			else
			{

				system("pause");
				return (EActionStatus::OK);
			}
		}
		else
		{
			// Handle simple commands that dont require any action like: swim, read, etc.
			if (searchCommand->gameEvent->getDamage() > 0)
			{
				player->takeDamage(searchCommand->gameEvent->getDamage(), analytics);// Subtract Damage from players life
			}
			system("pause");
			return (EActionStatus::OK);
		}
	}
	else
	{
		return (EActionStatus::Invalid_action);
	}
	return (EActionStatus::Invalid_action);
}

/////////////////////////////// ACTION FUNCTION POINTERS/////////////////////////////////////////////////////////////////
bool GameManager::pickup(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	Item* selectedItem = this->currentRoom->itemList.FindItemByName(completePredicate);// Search for the Item the player wants
	Weapon* selectedWeapon = this->currentRoom->weaponList.FindWeaponByName(completePredicate); // Search for the Item the player wants

	Chest* targetChest = nullptr;
	if (predicate.size() > 1)// To pickup from the chest user input must be: pickup chestname item
	{
		targetChest = this->currentRoom->chestList.FindChestByName(predicate[0]);// Look for the chestname in the chests
		if (targetChest != nullptr)
		{
			if (targetChest->getIsOpen())// See if the chest is open
			{
				completePredicate = completePredicate.substr(completePredicate.find_first_of(" \t") + 1);// Remove from the predicate the chest name
				selectedItem = targetChest->itemList.FindItemByName(completePredicate);// Search for the Item the player wants
				selectedWeapon = targetChest->weaponList.FindWeaponByName(completePredicate); // Search for the Item the player wants
			}
		}
	}

	if (this->pickupItemFromTarget(selectedItem, targetChest))
	{
		this->analytics->increaseNumPickups();
		return true;
	}

	if (this->pickupWeaponFromTarget(selectedWeapon, targetChest))
	{
		this->analytics->increaseNumPickups();
		return true;
	}

	//No Item with that name found 
	return false;
}

bool GameManager::place(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	if (this->player->placeItem(completePredicate)) // Search for the Item the player wants to place
	{
		// Item placed sucessfully and the command is removed
		currentRoom->commandList.removeCommand(searchCommand);
		return true;
	}
	else
	{
		// Item not found
		return false;
	}
}

bool GameManager::equip(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	if (this->player->equipWeapon(completePredicate)) // Search for the Weapon the player wants to equip and equip it
	{
		// Weapon equipped
		return true;
	}
	else
	{
		// No Weapon found
		return false;
	}
}

bool GameManager::go(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	std::string roomName = searchCommand->getGoToRoom(); // Gets the room name from the command in the XML
	if (roomName != "")
	{
		Room* nextRoom = this->LoadRoomByName(roomName.c_str()); // Change room to the new room provided by the XML command list
		if (this->activeRooms[roomName]) // Check if the next room is open for the player to access
		{

			this->currentRoom = nextRoom;

			system("pause");

			std::cout << std::endl << std::endl;
			this->currentRoom->display();
			return true;
		}
		else
		{
			std::cout << "You can't access the door, it seems to be blocked" << std::endl << std::endl;
			return true;
		}
	}
	return true;
}

bool GameManager::help(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	this->currentRoom->commandList.display();// Display all commands avaiable
	return true;
}

bool GameManager::look(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	this->currentRoom->display();// Display all the things inside the room
	return true;
}

bool GameManager::save(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	player->save(fpSavePlayer);
	currentRoom->save(fpSaveLevel);

	std::cout << "DATA SAVED" << std::endl << std::endl;
	return true;
}

bool GameManager::load(Command * searchCommand, std::vector<std::string>& predicate, std::string & completePredicate)
{
	initialize(fpSaveLevel);
	player->initialize(fpSavePlayer);

	std::cout << "DATA LOADED" << std::endl << std::endl;
	return true;
}

bool GameManager::HandleTrigger(Command* searchCommand, std::string &trigger, std::string &key)
{
	// Had changed it to function pointers, but I started to get really weid errors when using transfrom for changing strings to lower case :/
	if (trigger == "SurpriseAttack")
	{
		std::string targetName = searchCommand->gameEvent->getTarget();
		Enemy* enemy = currentRoom->enemyList.FindEnemyByName(targetName);
		if (enemy != nullptr)
		{
			searchCommand->gameEvent->display();// Display the event
			enemy->display();//Display enemie info
			battle(enemy);
			return true;
		}
		return true;
	}

	if (trigger == "OpenChest")
	{
		std::string targetName = searchCommand->gameEvent->getTarget();
		Chest* targetChest = currentRoom->chestList.FindChestByName(targetName);
		if (!targetChest->getIsOpen())
		{
			targetChest->OpenChest(key.c_str());
			return true;
		}
	}

	if (trigger == "OpenDoor")
	{
		activeRooms[searchCommand->gameEvent->getTarget()] = true;
		searchCommand->gameEvent->display();
		return true;
	}

	searchCommand->gameEvent->display();
	if (searchCommand->gameEvent->getDamage() > 0) // Check if the command damages the player
	{
		this->player->takeDamage(searchCommand->gameEvent->getDamage(), this->analytics);// Subtract Damage from players life
	}
	return false;
}

bool GameManager::pickupItemFromTarget(Item * selectedItem, Chest * targetChest)
{
	if (player->pickUpItem(selectedItem)) // If player succesfully added it to his inventory
	{
		if (targetChest != nullptr)// If its target is from a chest
		{
			targetChest->itemList.removeItem(selectedItem);
		}
		else// If its target is from the room
		{
			currentRoom->itemList.removeItem(selectedItem);
		}

		//If item gives player a boost to health;
		player->addToHealth(selectedItem->getEffect());

		std::cout << "You pickup:" << std::endl;
		selectedItem->display();
		return true;
	}
	return false;
}

bool GameManager::pickupWeaponFromTarget(Weapon * selectedWeapon, Chest * targetChest)
{
	if (player->pickUpWeapon(selectedWeapon))// If player succesfully added it to his inventory
	{
		if (targetChest != nullptr)// If its target is from a chest
		{
			targetChest->weaponList.removeWeapon(selectedWeapon);
		}
		else// If its target is from the room
		{
			currentRoom->weaponList.removeWeapon(selectedWeapon);
		}
		std::cout << "You pickup:" << std::endl;
		selectedWeapon->display();
		return true;
	}
	return false;
}

void GameManager::dropOnDeathInventory()
{
	for (auto *item : player->itemList.items)// Leave all player items in the room
	{
		currentRoom->itemList.addItem(item);
	}
	player->itemList.items.clear();

	for (auto *weapon : player->weaponList.weapons)// Leave all player weapons in the room
	{
		currentRoom->weaponList.addWeapon(weapon);
	}
	player->weaponList.weapons.clear();
}

void GameManager::Reset()
{
	if (player->isPlayerDead())
	{
		dropOnDeathInventory(); // Drop player loot in level
	}
	else
	{
		activeRooms.clear();//Clear all active rooms
		for (auto room : createdRooms)//Clear all current rooms
		{
			if (room.second->getName() != currentRoom->getName())
			{
				delete(room.second);
				room.second = nullptr;
			}
		}
		createdRooms.clear();
	}
	
	if (!initialize(fpSaveLevel) || !player->initialize(fpSavePlayer))// Initialize to saved files if it exists
	{
		system("CLS");
		initialize(fpStart);
	}
	analytics->increaseNumPlays();//Increase the number of plays

	//Reset player
	Player * newPlayer = new Player();
	delete(player);
	player = newPlayer;
}



