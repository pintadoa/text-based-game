#pragma once
#include "Enemy.h"
#include <algorithm>
#include<list>

// Class containing a dinamic list of Enemies 
class EnemyList
{
public:
	std::list <Enemy*> enemies;//List Containing enemies 

	EnemyList();
	~EnemyList();
	void initialize(XMLElement * element);//Loading all chests from XML
	void save(XMLElement * element);//Save all chests to XML
	void addEnemy(Enemy* enemy);
	void removeEnemy(Enemy* enemy);
	Enemy* FindEnemyByName(std::string& name);//Finds an enemy by name and returns a pointer to the object
};
