#pragma once
#include "Weapon.h"
#include <algorithm>
#include <list>

//Class containing a dynamic list of weapons
class WeaponList
{
public:
	std::list <Weapon*> weapons;//List Containing Weapons 
	
	WeaponList();
	~WeaponList();
	void initialize(XMLElement * element);//Loading all weapons from XML
	void save(XMLElement* element);
	void addWeapon(Weapon* weapon);
	void removeWeapon(Weapon* weapon);
	void display();
	Weapon* FindWeaponByName(std::string& name);//Finds a weapon by name and returns a pointer to the object
};

