#include "Analytics.h"



Analytics::Analytics() : numActions(0), numPickup(0), numPlays(1), numDamage(0)
{
}


Analytics::~Analytics()
{
}

void Analytics::initialize()
{
	XMLDocument* doc = new XMLDocument();
	doc->LoadFile(fpAnalytics.c_str());
	if (doc->Error() != XML_SUCCESS)
	{
		delete(doc);
		save();
		initialize();
		return;
	}

	XMLElement * analyticsElement = doc->FirstChildElement("Analytics");

	XMLElement * numActionsElement = analyticsElement->FirstChildElement("numActions");
	if (numActionsElement != nullptr)
	{
		numActionsElement->QueryIntText(&numActions);
	}

	XMLElement * numPickupElement = analyticsElement->FirstChildElement("numPickup");
	if (numPickupElement != nullptr)
	{
		numPickupElement->QueryIntText(&numPickup);
	}

	XMLElement * numPlaysElement = analyticsElement->FirstChildElement("numPlays");
	if (numPlaysElement != nullptr)
	{
		numPlaysElement->QueryIntText(&numPlays);
	}

	XMLElement * numDamageElement = analyticsElement->FirstChildElement("numDamage");
	if (numDamageElement != nullptr)
	{
		numDamageElement->QueryIntText(&numDamage);
	}
	delete(doc);
}

void Analytics::save()
{
	XMLDocument* doc = new XMLDocument();

	XMLElement* analyticsElement = doc->NewElement("Analytics");

	//Save numActions
	XMLElement* numActionsElement = analyticsElement->GetDocument()->NewElement("numActions");
	numActionsElement->SetText(numActions);
	analyticsElement->LinkEndChild(numActionsElement);

	//Save numActions
	XMLElement* numPickupElement = analyticsElement->GetDocument()->NewElement("numPickup");
	numPickupElement->SetText(numPickup);
	analyticsElement->LinkEndChild(numPickupElement);

	//Save numActions
	XMLElement* numPlaysElement = analyticsElement->GetDocument()->NewElement("numPlays");
	numPlaysElement->SetText(numPlays);
	analyticsElement->LinkEndChild(numPlaysElement);

	//Save numActions
	XMLElement* numDamageElement = analyticsElement->GetDocument()->NewElement("numDamage");
	numDamageElement->SetText(numDamage);
	analyticsElement->LinkEndChild(numDamageElement);

	//Insert to doc for saving
	doc->InsertEndChild(analyticsElement);

	doc->SaveFile(fpAnalytics.c_str());
	if (doc->Error() != XML_SUCCESS)
	{
		std::cout << "ERROR SAVING: Analytics Files did not save" << std::endl;
		delete(doc);
		return;
	}
	delete(doc);
}

void Analytics::increaseNumActions()
{
	numActions++;
}

void Analytics::increaseNumPickups()
{
	numPickup++;
}

void Analytics::increaseNumPlays()
{
	numPlays++;
}

void Analytics::increaseNumDamage()
{
	numDamage++;
}
