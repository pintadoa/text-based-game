#pragma once
#include "Command.h"
#include <algorithm>
#include <list>

// Class containing a dinamic list of Commands 
class CommandList
{	
	std::list<Command*> commands;// List of commands the user can use in the room
public:
	std::list<std::string> publicCommands { "pickup", "equip", "look","help","save", "load", "exit"}; // Commands avaiable to all rooms
	CommandList();
	~CommandList();
	void initialize(XMLElement * element);//Load xml of commands related to action
	void save(XMLElement * element);//Save xml of commands 
	void addCommand(Command* command);
	void removeCommand(Command* command);
	void display();
	Command* SearchByAction(std::string & action, std::string & predicate);// Searches the commands list for one with the same action key word given by the user
};

